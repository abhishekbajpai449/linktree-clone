"use client";

import { signIn } from "next-auth/react";
import { useRouter } from "next/navigation";

export function HeroForm({ session }) {
  const router = useRouter();
  const formSubmitHandler = async (e) => {
    e.preventDefault();
    const form = e.target;
    const input = form.querySelector("input");
    const desiredUsername = input.value;
    const { user } = session;
    if (user?.name) {
      router.push("/account?username=" + desiredUsername);
    } else {
      await signIn("google", {
        callbackUrl: `/account?username=${desiredUsername}`,
      });
    }
  };

  return (
    <form
      className="inline-flex items-center shadow-lg shadow-gray-500/20"
      onSubmit={formSubmitHandler}
    >
      <span className="font-bold bg-white py-4 pl-4">{"linklist.to/"}</span>
      <input type="text" placeholder="yourLink" className="py-4"></input>
      <button type="submit" className="bg-blue-600 px-6 py-4">
        Join For Free
      </button>
    </form>
  );
}
