"use client";

import { RightArrowIcon } from "@/components/icons/RightArrowIcon";
import { createPage } from "@/actions/createPage";
import { useState } from "react";
import { redirect } from "next/navigation";
import SubmitButton from "../buttons/SubmitButton";

export default function UsernameForm({ username }) {
  const [taken, setTaken] = useState(false);

  const handleClaimUsernameSubmit = async (formData) => {
    const page = await createPage(formData);
    setTaken(page === false);
    if (page) {
      redirect(`/account/?created=${formData.get("username")}`);
    }
  };

  return (
    <form
      action={handleClaimUsernameSubmit}
      className="flex flex-col gap-2 p-4"
    >
      <input
        type="text"
        placeholder="username"
        name="username"
        className="w-full p-2 text-center"
        defaultValue={username}
      ></input>
      {taken && (
        <div className="bg-red-200 border border-red-500  text-center p-2 w-full">
          This username already taken
        </div>
      )}
      <SubmitButton
        type="submit"
        className="bg-blue-500 disabled:bg-blue-300 text-white disabled:text-gray-200 p-2 w-full flex items-center justify-center gap-2"
      >
        <span>Claim your username</span>
        <RightArrowIcon></RightArrowIcon>
      </SubmitButton>
    </form>
  );
}
