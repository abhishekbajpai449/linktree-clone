"use client";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGoogle } from "@fortawesome/free-brands-svg-icons";
import { signIn } from "next-auth/react";

export default function LogInWithGoogle() {
  return (
    <button
      className="bg-white border px-6 py-4 w-full flex justify-center items-center gap-2"
      onClick={() => {
        console.log("Clicked!!");
        signIn("google");
      }}
    >
      <FontAwesomeIcon icon={faGoogle} className="h-6 w-6" />
      <span>Sign in with google</span>
    </button>
  );
}
