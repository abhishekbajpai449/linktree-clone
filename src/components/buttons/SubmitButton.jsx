import { useFormStatus } from "react-dom";
export default function SubmitButton({ children, className }) {
  const { pending } = useFormStatus();
  return (
    <button type="submit" className={className} disabled={pending}>
      {children}
    </button>
  );
}
