"use client";

import { faRightFromBracket } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { signOut } from "next-auth/react";

export default function LogoutButton({ className, iconClassName }) {
  return (
    <>
      <button
        className={`border flex gap-2 p-2 items-center ${className}`}
        onClick={() => {
          signOut({ callbackUrl: "/" });
        }}
      >
        <FontAwesomeIcon
          icon={faRightFromBracket}
          className={iconClassName}
        ></FontAwesomeIcon>
        <span>Logout</span>
      </button>
    </>
  );
}
