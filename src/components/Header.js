import { authOptions } from "@/app/api/auth/[...nextauth]/route";
import { getServerSession } from "next-auth";
import Link from "next/link";
import LogoutButton from "./buttons/LogoutButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLink } from "@fortawesome/free-solid-svg-icons";

export const Header = async () => {
  const session = await getServerSession(authOptions);

  return (
    <header className="p-4 bg-white border-b">
      <div className="flex flex-row mx-auto gap-6 items-center">
        <Link
          href="/"
          className="font-extrabold flex items-center gap-2 text-blue-700"
        >
          <FontAwesomeIcon icon={faLink}></FontAwesomeIcon>
          <span>LinkList</span>
        </Link>
        <div className="w-11/12 flex justify-between items-center text-sm text-slate-500">
          <nav className="flex gap-4">
            <Link href="#">About</Link>
            <Link href="#">Pricing</Link>
            <Link href="#">Contact Us</Link>
          </nav>
          <nav className="flex gap-4 items-center">
            {!session && (
              <>
                <Link href={"/login"}>Sign In</Link>
                <Link href="#">Create Account</Link>
              </>
            )}

            {!!session && (
              <>
                <span>{session.user?.name}</span>
                <LogoutButton></LogoutButton>
              </>
            )}
          </nav>
        </div>
      </div>
    </header>
  );
};
