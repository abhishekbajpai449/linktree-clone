"use client";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft, faChartLine } from "@fortawesome/free-solid-svg-icons";
import { faFileLines } from "@fortawesome/free-regular-svg-icons";
import LogoutButton from "@/components/buttons/LogoutButton";
import Link from "next/link";
import { usePathname } from "next/navigation";

export default function AppSideBar() {
  const path = usePathname();

  return (
    <nav className="flex flex-col mt-8 mx-auto gap-2 w-fit text-gray-700">
      <Link
        href="/account"
        className={`flex gap-4 p-2 ${
          path === "/account" ? "text-blue-500" : ""
        }`}
      >
        <FontAwesomeIcon icon={faFileLines} className="h-6 w-6" />
        <span>My Page</span>
      </Link>
      <Link
        href="/analytics"
        className={`flex gap-4 p-2 ${
          path === "/analytics" ? "text-blue-500" : ""
        }`}
      >
        <FontAwesomeIcon icon={faChartLine} className="h-6 w-6" />
        <span>Analytics</span>
      </Link>
      <LogoutButton
        className="gap-4 border-0 text-gray-500"
        iconClassName="h-6"
      ></LogoutButton>
      <Link
        href="/"
        className="flex gap-2 items-center text-xs text-gray-500 border-t pt-4"
      >
        <FontAwesomeIcon
          icon={faArrowLeft}
          className="w-3 h-3"
        ></FontAwesomeIcon>
        <span>Back to website</span>
      </Link>
    </nav>
  );
}
