const { Schema, model, models } = require("mongoose");

const pageSchema = new Schema(
  {
    uri: { type: String, unique: true, required: true, min: 1 },
    owner: { type: String, required: true },
  },
  { timestamps: true }
);

export const Page = models?.Page || new model("Page", pageSchema);
