"use server";

import { authOptions } from "@/app/api/auth/[...nextauth]/route";
import { Page } from "@/models/Page";
import mongoose from "mongoose";
import { getServerSession } from "next-auth";

export const createPage = async (formData) => {
  const username = formData.get("username");
  await mongoose.connect(process.env.MONGODB_URI);
  const pageExits = await Page.findOne({ uri: username });
  if (pageExits) return false;
  const session = await getServerSession(authOptions);
  const createdPage = await Page.create({
    uri: username,
    owner: session?.user?.email,
  });
  return createdPage.toObject();
};
