import LoginWithGoogle from "../../../components/buttons/LoginWithGoogle";

export default function LoginPage() {
  return (
    <div className="max-w-xs mx-auto p-4">
      <h1 className="text-4xl font-bold text-center mb-1">Sing In</h1>
      <p className="text-center text-gray-500 mb-6">
        Sign in to your account using one of the methods below
      </p>
      <LoginWithGoogle></LoginWithGoogle>
    </div>
  );
}
