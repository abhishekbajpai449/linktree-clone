import { HeroForm } from "@/components/forms/HeroForm";
import { authOptions } from "../api/auth/[...nextauth]/route";
import { getServerSession } from "next-auth";

export default async function Home() {
  const session = await getServerSession(authOptions);
  return (
    <section className="pt-32">
      <div className="max-w-md mb-8">
        <h1 className="text-6xl font-bold mb-4">
          Your one link
          <br />
          for everything
        </h1>
        <h2 className="text-gray-500 text-xl">
          Share your links, social profiles, contact info and more on one page
        </h2>
      </div>

      <HeroForm session={session}></HeroForm>
    </section>
  );
  // </main>
}
