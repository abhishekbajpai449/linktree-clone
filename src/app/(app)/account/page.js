import { getServerSession } from "next-auth";
import { authOptions } from "../../api/auth/[...nextauth]/route";
import { redirect } from "next/navigation";
import UsernameForm from "../../../components/forms/UsernameForm";
import { Page } from "@/models/Page";
import mongoose from "mongoose";

export default async function AccountPage(req) {
  const session = await getServerSession(authOptions);
  const { username } = req.searchParams;
  if (!session) {
    return redirect("/");
  }
  await mongoose.connect(process.env.MONGODB_URI);
  const page = await Page.findOne({ owner: session.user.email });
  if (page) {
    return <>Your page is {page.uri}</>;
  }

  return (
    <div className="mx-auto max-w-xs">
      <h1 className="font-bold text-3xl mb-2">Grab your username</h1>
      <h4 className="text-sm text-gray-700 text-center">
        Choose your username
      </h4>
      <UsernameForm username={username}></UsernameForm>
    </div>
  );
}
