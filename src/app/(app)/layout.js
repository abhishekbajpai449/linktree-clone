import { Lato } from "next/font/google";
import "../globals.css";
import { getServerSession } from "next-auth";
import { authOptions } from "../api/auth/[...nextauth]/route";
import { redirect } from "next/navigation";
import Image from "next/image";
import AppSideBar from "@/components/layout/AppSideBar";

const lato = Lato({ subsets: ["latin"], weight: ["400", "700"] });

export default async function AppLayout({ children }) {
  const session = await getServerSession(authOptions);
  if (!session) {
    redirect("/");
  }
  return (
    <html lang="en">
      <body className={lato.className}>
        <main className="flex min-h-screen">
          <aside className="bg-white max-w-md w-48 p-4 shadow">
            <div className="rounded-full overflow-hidden w-24 mx-auto">
              <Image
                src={session.user.image}
                alt="avatar"
                width="256"
                height="256"
              ></Image>
            </div>
            <AppSideBar></AppSideBar>
          </aside>
          <div className="grow">
            <div className="bg-white p-4 m-8">{children}</div>
          </div>
        </main>
      </body>
    </html>
  );
}
